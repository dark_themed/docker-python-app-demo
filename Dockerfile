ARG PYTHON_IMAGE='3.9-alpine'
FROM python:${PYTHON_IMAGE}
WORKDIR /app
COPY src src/
COPY tests tests/
COPY requirements.txt test-requirements.txt ./
RUN pip3 install -r requirements.txt -r test-requirements.txt
RUN pytest -vv tests/demo.py
FROM python:${PYTHON_IMAGE}
EXPOSE 5000
WORKDIR /app
COPY src src/
COPY start.sh requirements.txt ./
COPY requirements.txt .
RUN pip3 install -r requirements.txt
CMD /app/start.sh