import pytest


@pytest.mark.parametrize("arg1, arg2",
                         [(True, False), (True, True)])
def test_booleans(arg1, arg2):
    assert arg1 or arg2 is True
